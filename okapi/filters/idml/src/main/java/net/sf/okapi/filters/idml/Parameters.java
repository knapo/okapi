/*===========================================================================
  Copyright (C) 2009-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.idml;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.SpinInputPart;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider, ISimplifierRulesParameters {

	private static final String EXTRACTNOTES = "extractNotes";
	private static final String SIMPLIFYCODES = "simplifyCodes";
	private static final String EXTRACTMASTERSPREADS = "extractMasterSpreads";
	private static final String SKIPTHRESHOLD = "skipThreshold";
	private static final String STOPWHENOVERTHRESHOLD = "stopWhenOverThreshold";
	private static final String NEWTUONBR = "newTuOnBr";
	private static final String EXTRACTHIDDENLAYERS = "extractHiddenLayers";

	public Parameters () {
		super();
	}
	
	public void reset () {
		super.reset();
		setExtractNotes(false);
		setSimplifyCodes(true);
		setExtractMasterSpreads(true);
		setSkipThreshold(2000);
		setStopWhenOverThreshold(true);
		setNewTuOnBr(false);
		setExtractHiddenLayers(false);
		setSimplifierRules(null);
	}

	public boolean getExtractNotes () {
		return getBoolean(EXTRACTNOTES);
	}
	
	public void setExtractNotes (boolean extractNotes) {
		setBoolean(EXTRACTNOTES, extractNotes);
	}

	public boolean getSimplifyCodes () {
		return getBoolean(SIMPLIFYCODES);
	}
	
	public void setSimplifyCodes (boolean simplifyCodes) {
		setBoolean(SIMPLIFYCODES, simplifyCodes);
	}

	public boolean getExtractMasterSpreads () {
		return getBoolean(EXTRACTMASTERSPREADS);
	}
	
	public void setExtractMasterSpreads (boolean extractMasterSpreads) {
		setBoolean(EXTRACTMASTERSPREADS, extractMasterSpreads);
	}
	
	public int getSkipThreshold () {
		return getInteger(SKIPTHRESHOLD);
	}
	
	public void setSkipThreshold (int skipThreshold) {
		setInteger(SKIPTHRESHOLD, skipThreshold);
	}

	public boolean getStopWhenOverThreshold () {
		return getBoolean(STOPWHENOVERTHRESHOLD);
	}
	
	public void setStopWhenOverThreshold (boolean stopWhenOverThreshold) {
		setBoolean(STOPWHENOVERTHRESHOLD, stopWhenOverThreshold);
	}
	
	public boolean getNewTuOnBr () {
		return getBoolean(NEWTUONBR);
	}
	
	public void setNewTuOnBr (boolean newTuOnBr) {
		setBoolean(NEWTUONBR, newTuOnBr);
	}

	public boolean getExtractHiddenLayers () {
		return getBoolean(EXTRACTHIDDENLAYERS);
	}
	
	public void setExtractHiddenLayers (boolean extractHiddenLayers) {
		setBoolean(EXTRACTHIDDENLAYERS, extractHiddenLayers);
	}
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(EXTRACTNOTES, "Extract notes", null);
		desc.add(EXTRACTMASTERSPREADS, "Extract master spreads", null);
		desc.add(EXTRACTHIDDENLAYERS, "Extract hidden layers", null);
		desc.add(SIMPLIFYCODES, "Simplify inline codes when possible", null);
		desc.add(NEWTUONBR, "Create new text units on hard returns [IMPORTANT: STILL BETA! MAY PREVENT MERGING BACK!]",
			"Option is BETA and may prevent you to merge back. Make sure to test round-trip!");
		desc.add(SKIPTHRESHOLD, "Maximum spread size", "Skip (or stop for) any spread larger than the given value (in Kbytes)");
		desc.add(STOPWHENOVERTHRESHOLD, "Generate an error when a spread is larger than the specified value", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("IDML Filter", true, false);
		
		desc.addCheckboxPart(paramsDesc.get(EXTRACTNOTES));
		desc.addCheckboxPart(paramsDesc.get(EXTRACTMASTERSPREADS));
		desc.addCheckboxPart(paramsDesc.get(EXTRACTHIDDENLAYERS));
		desc.addSeparatorPart();
		
		desc.addCheckboxPart(paramsDesc.get(SIMPLIFYCODES));
		desc.addCheckboxPart(paramsDesc.get(NEWTUONBR));
		desc.addSeparatorPart();
		
		SpinInputPart sip = desc.addSpinInputPart(paramsDesc.get(SKIPTHRESHOLD));
		sip.setRange(1, 32000);
		desc.addCheckboxPart(paramsDesc.get(STOPWHENOVERTHRESHOLD));
		
		return desc;
	}

}
