/*===========================================================================
  Copyright (C) 2009-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.idml.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.filters.idml.IDMLFilter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class IDMLFilterTest {

	private IDMLFilter filter;
	private FileLocation root;
	private LocaleId locEN = LocaleId.fromString("en");

	@Before
	public void setUp() {
		filter = new IDMLFilter();
		root = FileLocation.fromClass(this.getClass());
	}

	@Test
	public void testDefaultInfo () {
		assertNotNull(filter.getParameters());
		assertNotNull(filter.getName());
		List<FilterConfiguration> list = filter.getConfigurations();
		assertNotNull(list);
		assertTrue(list.size()>0);
	}
	
	@Test
	public void testSimpleEntry () {
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents("/helloworld-1.idml"), 1);
		assertNotNull(tu);
		String text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
		assertEquals("Hello World!", text);
	}

    @Test
    public void testWhitespaces() {

        List<ITextUnit> iTextUnits = FilterTestDriver.filterTextUnits(getEvents("/tabsAndWhitespaces.idml"));
        assertNotNull(iTextUnits);
        assertEquals(7, iTextUnits.size());

        TextUnit tu = (TextUnit) iTextUnits.get(0);
        String text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("Hello World."
                + "Hello\tWorld with a Tab."
                + "Hello \tWorld with a Tab and a white space.", text);


        tu = (TextUnit) iTextUnits.get(1);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getText());
        assertEquals("Hello World\t.Hello World.", text);

        tu = (TextUnit) iTextUnits.get(2);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("Hello      World.", text);

        tu = (TextUnit) iTextUnits.get(3);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals(" Hello World\t.", text);

        tu = (TextUnit) iTextUnits.get(4);
        assertFalse(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("HelloWorldwithout.", text);

        tu = (TextUnit) iTextUnits.get(5);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("Hello \tWorld with a Tab and a white space.", text);

        tu = (TextUnit) iTextUnits.get(6);
        assertTrue(tu.preserveWhitespaces());
        text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
        assertEquals("m-space\u2003here."
                + "n-space\u2002here."
                + "another m-spacehere." // the \u2009 is removed and changed to a code
                //                          TagType.PLACEHOLDER, "sp-thin"
                + "another one\u00a0here.", text);
    }

    @Test
    public void testNewline() {
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents("/newline.idml"), 1);
        assertNotNull(tu);
        TextFragment firstContent = tu.getSource().getFirstContent();
        String text = TextFragment.getText(firstContent.getCodedText());
        assertEquals("32Hello World", text);
    }

	@Test
	public void testSimpleEntry2 () {
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents("/Test00.idml"), 1);
		assertNotNull(tu);
		String text = TextFragment.getText(tu.getSource().getFirstContent().getCodedText());
		assertEquals("Hello World!", text);
	}

	@Test
	public void testStartDocument () {
		assertTrue("Problem in StartDocument", FilterTestDriver.testStartDocument(filter,
			new InputDocument(root.in("/Test01.idml").toString(), null),
			"UTF-8", locEN, locEN));
	}
	
	@Test
	public void testDoubleExtraction () {
		// Read all files in the data directory
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		
//		list.add(new InputDocument(root+"Test03.idml", "okf_idml@ExtractAll-BreakOnBR.fprm"));
		
		list.add(new InputDocument(root.in("/Test00.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/Test01.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/Test02.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/helloworld-1.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/ConditionalText.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/Test03.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/testWithSpecialChars.idml").toString(), "okf_idml@ExtractAll.fprm"));

		list.add(new InputDocument(root.in("/TextPathTest01.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/TextPathTest02.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/TextPathTest03.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/TextPathTest04.idml").toString(), "okf_idml@ExtractAll.fprm"));
		
		list.add(new InputDocument(root.in("/idmltest.idml").toString(), "okf_idml@ExtractAll.fprm"));
		list.add(new InputDocument(root.in("/idmltest.idml").toString(), null));

		RoundTripComparison rtc = new RoundTripComparison(false); // Do not compare skeleton
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN, "output"));
	}

	private ArrayList<Event> getEvents (String testFileName) {
		return FilterTestDriver.getEvents(filter, new RawDocument(root.in(testFileName).asUri(), "UTF-8", locEN), null);
	}

}
