/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.languagetool;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.JLanguageTool;
import org.languagetool.Language;
import org.languagetool.Languages;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.tokenizers.WordTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.LocaleId;

/**
 * Helper methods for working with the LanguageTool API's
 * 
 * @author jimh
 *
 */
public class LanguageToolUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(LanguageToolUtil.class);

	private static ConcurrentMap<String, Language> languageCache = new ConcurrentHashMap<>();

	/**
	 * Thread-safe cache for {@link Language} objects. These objects should be thread safe
	 * and can be reused across {@link JLanguageTool} instances.
	 * @param locale 
	 * @return a cached {@link Language}
	 */
	public static Language getCachedLanguage(LocaleId locale) {
		// do an atomic get, first-check.
		Language language = languageCache.get(locale.toBCP47());
		if (language == null) {
			// There's a good chance we're the first thread to request this
			// settings
			// be optimistic, and assume we are the the only one.
			language = getLanguage(locale);
			// atomic second-check
			Language race = languageCache.putIfAbsent(locale.toBCP47(), language);
			if (race != null) {
				// some other thread managed to do the double-check at the same
				// time as us, and win the race to the putIfAbsent
				// use the winner's Connector, (and abandon ours).
				if (language instanceof Closeable) {
					try {
						((Closeable)language).close();
						language = null;
					} catch (Exception e) {
						LOGGER.error("Language couldn't be closed. Possible memory leak.", locale.toBCP47(), e);
					}
				}
				// return the winner
				return race;
			}
		}
		LOGGER.debug("LanguageTool Language cache hit for {}", locale.toBCP47());
		return language;
	}

	/**
	 * Gets the LT language code for the given locale Id
	 * 
	 * @param locale
	 *            the locale id to map.
	 * @return the LT language code, or null if the locale id could not be
	 *         mapped.
	 */
	public static final Language getLanguage(LocaleId locale) {
		// if the locale is only "en" default to American English
		if (locale == LocaleId.ENGLISH) {
			return new AmericanEnglish();
		}
		
		// try full locale with language, country and possibly a variant code
		if (Languages.isLanguageSupported(locale.toBCP47())) {
			return Languages.getLanguageForLocale(locale.toJavaLocale());
		// else try just the language code
		} else if (Languages.isLanguageSupported(locale.getLanguage())) {
			return Languages.getLanguageForLocale(Locale.forLanguageTag(locale.getLanguage()));
		// otherwise use the DefaultLanguage with universal rules
		} else {
			return new DefaultLanguage();
		}
	}

	/**
	 * Tokenize using LanguageTool tokenizers
	 * 
	 * @param text
	 *            text to tokenize
	 * @param lang
	 *            The {@link Language} from which we get the
	 *            {@link WordTokenizer}
	 * @return list of tokens with all whitespace trimmed
	 * @throws IOException
	 */
	public static final List<AnalyzedTokenReadings> tokenize(String text, Language lang) throws IOException {
		final List<String> tokens = lang.getWordTokenizer().tokenize(text);
		final List<AnalyzedTokenReadings> aTokens = lang.getTagger().tag(tokens);
		return aTokens;
	}
}
