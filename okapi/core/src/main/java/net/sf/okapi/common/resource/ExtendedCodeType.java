/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

/**
 * Provides extended code types.
 */
public enum ExtendedCodeType {

    COLOR("color"),
    HIGHLIGHT("highlight"),
    SHADE("shade"),
    SHADOW("shadow"),
    STRIKE_THROUGH("strikethrough"),
    UNDERLINE("underline"),
    SUPERSCRIPT("sup"),
    SUBSCRIPT("sub");

    String value;

    ExtendedCodeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
